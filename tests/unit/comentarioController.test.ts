import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';

import { Result } from '../../src/core/logic/Result';

import IPostService from "../../src/services/IServices/IPostService";
import PostController from "../../src/controllers/postController";
import IPostDTO from '../../src/dto/IPostDTO';

import IComentarioService from "../../src/services/IServices/IComentarioService";
import ComentarioController from "../../src/controllers/comentarioController";
import IComentarioDTO from '../../src/dto/IComentarioDTO';

describe('comentario controller', function () {
    var sinon = require('sinon');
	beforeEach(function() {
		sinon.restore();
    });

    it('createComentario: returns json with email+text+post values', async function () {
		this.timeout(20000);

        // criar post
        let bodyP = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let reqP: Partial<Request> = {};
		reqP.body = bodyP;

        let resP: Partial<Response> = {
			json: sinon.spy()
        };
		let nextP: Partial<NextFunction> = () => {};


		let postSchemaInstance = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance);

		let postRepoClass = require("../../src/repos/postRepo").default;
		let postRepoInstance = Container.get(postRepoClass);
		Container.set("PostRepo", postRepoInstance);

		let postServiceClass = require("../../src/services/postService").default;
		let postServiceInstance = Container.get(postServiceClass);
		Container.set("PostService", postServiceInstance);

		postServiceInstance = Container.get("PostService");
		sinon.stub(postServiceInstance, "createPost").returns( Result.ok<IPostDTO>( {"id":reqP.body.id, "emailUser": reqP.body.emailUser, "text": reqP.body.text, "likes": reqP.body.likes, "dislikes": reqP.body.dislikes} ));

		const ctrlP = new PostController(postServiceInstance as IPostService);

		await ctrlP.createPost(<Request>reqP, <Response>resP, <NextFunction>nextP);

        // criar comentario
        let body = { "emailUser":'bea@gmail.com', "text":'post_test'};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
			json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};


		let comentarioSchemaInstance = require("../../src/persistence/schemas/comentarioSchema").default;
		Container.set("comentarioSchema", comentarioSchemaInstance);

		let comentarioRepoClass = require("../../src/repos/comentarioRepo").default;
		let comentarioRepoInstance = Container.get(comentarioRepoClass);
		Container.set("ComentarioRepo", comentarioRepoInstance);

		let comentarioServiceClass = require("../../src/services/comentarioService").default;
		let comentarioServiceInstance = Container.get(comentarioServiceClass);
		Container.set("ComentarioService", comentarioServiceInstance);

		comentarioServiceInstance = Container.get("ComentarioService");
		sinon.stub(comentarioServiceInstance, "createComentario").returns( Result.ok<IComentarioDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "post": reqP.body.id} ));

		const ctrl = new ComentarioController(comentarioServiceInstance as IComentarioService);

		await ctrl.createComentario(<Request>req, <Response>res, <NextFunction>next);

		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match({"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "post": reqP.body.id}));
	});

	afterEach(function () {
		sinon.restore();
	});
});