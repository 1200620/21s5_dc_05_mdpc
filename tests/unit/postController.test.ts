import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';

import { Result } from '../../src/core/logic/Result';

import IPostService from "../../src/services/IServices/IPostService";
import PostController from "../../src/controllers/postController";
import IPostDTO from '../../src/dto/IPostDTO';

describe('post controller', function () {
	
	var sinon = require('sinon');
	
	beforeEach(function() {
    });

	afterEach(function () {
		sinon.restore();
	});

    it('createPost: returns json with email+text+likes+dislikes values', async function () {
		this.timeout(20000);
        let body = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
			json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};


		let postSchemaInstance = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance);

		let postRepoClass = require("../../src/repos/postRepo").default;
		let postRepoInstance = Container.get(postRepoClass);
		Container.set("PostRepo", postRepoInstance);

		let postServiceClass = require("../../src/services/postService").default;
		let postServiceInstance = Container.get(postServiceClass);
		Container.set("PostService", postServiceInstance);

		postServiceInstance = Container.get("PostService");
		sinon.stub(postServiceInstance, "createPost").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		const ctrl = new PostController(postServiceInstance as IPostService);

		await ctrl.createPost(<Request>req, <Response>res, <NextFunction>next);

		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match({ "id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes}));
	});

	it('updatePost: returns json with email+text+likes+dislikes values', async function () {
		this.timeout(20000);

		let body = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
			json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};


		let postSchemaInstance = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance);

		let postRepoClass = require("../../src/repos/postRepo").default;
		let postRepoInstance = Container.get(postRepoClass);
		Container.set("PostRepo", postRepoInstance);

		let postServiceClass = require("../../src/services/postService").default;
		let postServiceInstance = Container.get(postServiceClass);
		Container.set("PostService", postServiceInstance);

		postServiceInstance = Container.get("PostService");
		sinon.stub(postServiceInstance, "createPost").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		const ctrl = new PostController(postServiceInstance as IPostService);

		await ctrl.createPost(<Request>req, <Response>res, <NextFunction>next);

		//alterações para post
        let body2 = { "emailUser":'bea2@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req2: Partial<Request> = {};
		req2.body = body2;

		let res2: Partial<Response> = {
			json: sinon.spy()
        };
		let next2: Partial<NextFunction> = () => {};

		let postSchemaInstance2 = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance2);

		let postRepoClass2 = require("../../src/repos/postRepo").default;
		let postRepoInstance2 = Container.get(postRepoClass2);
		Container.set("PostRepo", postRepoInstance2);

		let postServiceClass2 = require("../../src/services/postService").default;
		let postServiceInstance2 = Container.get(postServiceClass2);
		Container.set("PostService", postServiceInstance2);

		postServiceInstance2 = Container.get("PostService");
		let post = sinon.stub(postServiceInstance2, "updatePost").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req2.body.emailUser, "text": req2.body.text, "likes": req2.body.likes, "dislikes": req2.body.dislikes} ));

		const ctrl2 = new PostController(postServiceInstance2 as IPostService);

		await ctrl2.updatePost(<Request>req2, <Response>res2, <NextFunction>next2);

		post.callsFake(() => {
			sinon.assert.calledOnce(res2.json);
			sinon.assert.calledWith(res2.json, sinon.match({ "id":req.body.id, "emailUser": req2.body.emailUser, "text": req2.body.text, "likes": req2.body.likes, "dislikes": req2.body.dislikes}));
		  });

	});

	it('obter todos os posts', async function () {

		this.timeout(20000);
        let body = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
			//json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};


		let postSchemaInstance = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance);

		let postRepoClass = require("../../src/repos/postRepo").default;
		let postRepoInstance = Container.get(postRepoClass);
		Container.set("PostRepo", postRepoInstance);

		let postServiceClass = require("../../src/services/postService").default;
		let postServiceInstance = Container.get(postServiceClass);
		Container.set("PostService", postServiceInstance);

		postServiceInstance = Container.get("PostService");
		sinon.stub(postServiceInstance, "createPost").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		const ctrl = new PostController(postServiceInstance as IPostService);

		await ctrl.createPost(<Request>req, <Response>res, <NextFunction>next);

		//------------------ get all -----------------

		let req2: Partial<Request> = {};

		let res2: Partial<Response> = {
			json: sinon.spy()
        };

		let next2: Partial<NextFunction> = () => {};

		const ctrl2 = new PostController(postServiceInstance as IPostService);

		let getPosts = sinon.stub(postServiceInstance, "getAllPosts").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		await ctrl2.getAllPosts(<Request>req2, <Response>res2, <NextFunction>next2);

		getPosts.callsFake(() => {
			sinon.assert.calledOnce(res2.json);
			sinon.assert.calledWith(res2.json, sinon.match({ "id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes}));
		  });

	});

	it('obter posts de um utilizador', async function () {

		this.timeout(20000);
        let body = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req: Partial<Request> = {};
		req.body = body;

        let res: Partial<Response> = {
			//json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};


		let postSchemaInstance = require("../../src/persistence/schemas/postSchema").default;
		Container.set("postSchema", postSchemaInstance);

		let postRepoClass = require("../../src/repos/postRepo").default;
		let postRepoInstance = Container.get(postRepoClass);
		Container.set("PostRepo", postRepoInstance);

		let postServiceClass = require("../../src/services/postService").default;
		let postServiceInstance = Container.get(postServiceClass);
		Container.set("PostService", postServiceInstance);

		postServiceInstance = Container.get("PostService");
		sinon.stub(postServiceInstance, "createPost").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		const ctrl = new PostController(postServiceInstance as IPostService);

		await ctrl.createPost(<Request>req, <Response>res, <NextFunction>next);

		//------------------ get all -----------------

		let body2 = { "emailUser":'bea@gmail.com', "text":'post_test', "likes":[], "dislikes":[]};
        let req2: Partial<Request> = {};
		req.body = body2;

		let res2: Partial<Response> = {
			json: sinon.spy()
        };

		let next2: Partial<NextFunction> = () => {};

		const ctrl2 = new PostController(postServiceInstance as IPostService);

		let getPosts = sinon.stub(postServiceInstance, "getAllPosts").returns( Result.ok<IPostDTO>( {"id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes} ));

		await ctrl2.getAllPostsFromUser(<Request>req2, <Response>res2, <NextFunction>next2);

		getPosts.callsFake(() => {
			sinon.assert.calledOnce(res2.json);
			sinon.assert.calledWith(res2.json, sinon.match({ "id":req.body.id, "emailUser": req.body.emailUser, "text": req.body.text, "likes": req.body.likes, "dislikes": req.body.dislikes}));
		  });

	});
});