import { Request, Response, NextFunction } from 'express';

export default interface IComentarioController  {
  createComentario(req: Request, res: Response, next: NextFunction);
  updateComentario(req: Request, res: Response, next: NextFunction);
  getComentarioById(req:Request, res: Response, next:NextFunction);
  getAllComentarios(req:Request, res:Response,next:NextFunction);
  getAllComentariosFromPost(req:Request, res:Response,next:NextFunction);
}
