import { Request, Response, NextFunction } from 'express';

export default interface IPostController  {
  getPostById(req:Request, res: Response, next:NextFunction);
  createPost(req: Request, res: Response, next: NextFunction);
  updatePost(req: Request, res: Response, next: NextFunction);
  getAllPosts(req:Request, res:Response, next:NextFunction);
  getAllPostsFromUser(req:Request, res:Response, next:NextFunction);
}
