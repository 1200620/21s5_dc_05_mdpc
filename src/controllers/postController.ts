import { Inject, Service } from "typedi";
import { Request, Response, NextFunction } from 'express';
import IPostController from "./IControllers/IPostController";
import config from "../../config";
import IPostDTO from "../dto/IPostDTO";
import { Result } from "../core/logic/Result";
import IPostService from "../services/IServices/IPostService";

@Service()
export default class PostController implements IPostController/* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.post.name) private postServiceInstance: IPostService
  ) {}

  public async getPostById(req: Request, res: Response, next: NextFunction) {
    try{
      const postId = req.params.id;
      const postOrError = await this.postServiceInstance.getPost(postId) as Result<IPostDTO>;

      if (postOrError.isFailure) {
        return res.status(404).send();
      }

      const postDTO = postOrError.getValue();
      return res.status(201).json( postDTO );
    }catch (e){
      return next(e);
    }
  }

  public async createPost(req: Request, res: Response, next: NextFunction) {
    try{
      const postOrError = await this.postServiceInstance.createPost(req.body as IPostDTO) as Result<IPostDTO>;

      if(postOrError.isFailure){
        return res.status(402).send();
      }

      const postDto = postOrError.getValue();
      return res.json(postDto).status(201);
    }catch (e){
      return next(e);
    }
  }

  public async updatePost(req: Request, res: Response, next: NextFunction) {
    try {

      const id = req.params.id;
      const emailUser = req.body.emailUser;
      const text = req.body.text;
      const likes = req.body.likes;
      const dislikes = req.body.dislikes;

      const postOrError = await this.postServiceInstance.updatePost({id, emailUser, text, likes, dislikes} as IPostDTO) as Result<IPostDTO>;

      if (postOrError.isFailure) {
        return res.status(404).send();
      }

      const postDTO = postOrError.getValue();
      return res.status(201).json( postDTO );
    }
    catch (e) {
      return next(e);
    }
  }

  public async getAllPosts(req: Request, res: Response, next: NextFunction) {
    try {
      const postsOrError = await this.postServiceInstance.getAllPosts() as Result<IPostDTO[]>;
      if (postsOrError.isFailure) {
        return res.status(404).send();
      }
      const postsDto = postsOrError.getValue();
      return res.status(201).json(postsDto);
    } catch (e) {
      return next(e);
    }
  }

  public async getAllPostsFromUser(req: Request, res: Response, next: NextFunction) {
    try {
      const email = req.params.emailUser;
      const postsOrError = await this.postServiceInstance.getAllPostsUser(email) as Result<IPostDTO[]>;

      if (postsOrError.isFailure) {
        return res.status(404).send();
      }
      const postsDto = postsOrError.getValue();

      return res.status(201).json(postsDto);
    } catch (e) {
      return next(e);
    }
  }

}
