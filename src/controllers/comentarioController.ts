import {Inject, Service} from "typedi";
import config from "../../config";
import {NextFunction, Request, Response} from "express";
import {Result} from "../core/logic/Result";
import IComentarioController from "./IControllers/IComentarioController";
import IComentarioService from "../services/IServices/IComentarioService";
import IComentarioDTO from "../dto/IComentarioDTO";


@Service()
export default class ComentarioController implements IComentarioController/* TODO: extends ../core/infra/BaseController */ {
  constructor(
    @Inject(config.services.comentario.name) private comentarioServiceInstance: IComentarioService
  ) {}

  public async getComentarioById(req: Request, res: Response, next: NextFunction) {
    try {
      const comentarioDtoId = req.params.id;
      const comentarioOrError = await this.comentarioServiceInstance.getComentario(comentarioDtoId) as Result<IComentarioDTO>;

      if (comentarioOrError.isFailure) {
        return res.status(404).send();
      }

      const comentarioDTO = comentarioOrError.getValue();
      return res.status(201).json(comentarioDTO);
    } catch (e) {
      return next(e);
    }
  }

  public async createComentario(req: Request, res: Response, next: NextFunction) {
    try {
      const comentarioOrError = await this.comentarioServiceInstance.createComentario(req.body as IComentarioDTO) as Result<IComentarioDTO>;

      if (comentarioOrError.isFailure) {
        return res.status(402).send();
      }

      const comentarioDto = comentarioOrError.getValue();
      return res.json(comentarioDto).status(201);
    } catch (e) {
      return next(e);
    }
  }

  public async updateComentario(req: Request, res: Response, next: NextFunction) {
    try {
      const id = req.params.id;
      const emailUser = req.body.emailUser;
      const text = req.body.text;
      const comentarioOrError = await this.comentarioServiceInstance.updateComentario({
        id, emailUser, text
      } as IComentarioDTO) as Result<IComentarioDTO>;

      if (comentarioOrError.isFailure) {
        return res.status(404).send();
      }

      const comentarioDto = comentarioOrError.getValue();
      return res.status(201).json(comentarioDto);
    } catch (e) {
      return next(e);
    }
  }

  public async getAllComentarios(req: Request, res: Response, next: NextFunction) {
    try {
      const comentariosOrError = await this.comentarioServiceInstance.getAllComentarios();
      if (comentariosOrError.isFailure) {
        return res.status(404).send();
      }
      const comentariosDto = comentariosOrError.getValue();
      return res.status(201).json(comentariosDto);
    } catch (e) {
      return next(e);
    }
  }

  public async getAllComentariosFromPost(req: Request, res: Response, next: NextFunction) {
    try {
      const postId = req.params.postId;
      const comentariosOrError = await this.comentarioServiceInstance.getAllComentariosFromPost(postId);
      if (comentariosOrError.isFailure) {
        return res.status(404).send();
      }
      const comentariosDto = comentariosOrError.getValue();
      return res.status(201).json(comentariosDto);
    } catch (e) {
      return next(e);
    }
  }

}
