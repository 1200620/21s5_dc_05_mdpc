import { Repo } from "../../core/infra/Repo";
import { Post } from "../../domain/post";
import { PostId } from "../../domain/postId";

export default interface IPostRepo extends Repo<Post> {
  save(post: Post): Promise<Post>;
  findByDomainId (postId: PostId | string): Promise<Post>;

  findAll():Promise<Post[]>;
  findAllSpecificUser(email:string):Promise<Post[]>;
  //findByIds (PostsIds: PostId[]): Promise<Post[]>;
  //saveCollection (Posts: Post[]): Promise<Post[]>;
  //removeByPostIds (Posts: PostId[]): Promise<any>
}
