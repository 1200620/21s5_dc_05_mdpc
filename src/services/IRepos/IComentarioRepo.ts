import {Comentario} from "../../domain/comentario";
import {Repo} from "../../core/infra/Repo";
import {ComentarioId} from "../../domain/comentarioId";

export default interface IComentarioRepo extends Repo<Comentario> {
  save(comentario: Comentario): Promise<Comentario>;
  findByDomainId (comentarioId: ComentarioId | string): Promise<Comentario>;
  findAll():Promise<Comentario[]>;
  findAllFromPost(postId:string):Promise<Comentario[]>;

  //findByIds (PostsIds: PostId[]): Promise<Post[]>;
  //saveCollection (Posts: Post[]): Promise<Post[]>;
  //removeByPostIds (Posts: PostId[]): Promise<any>
}
