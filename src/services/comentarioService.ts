import {Inject, Service} from "typedi";
import config from "../../config";
import {Result} from "../core/logic/Result";
import IComentarioService from "./IServices/IComentarioService";
import IComentarioDTO from "../dto/IComentarioDTO";
import IComentarioRepo from "./IRepos/IComentarioRepo";
import {Comentario} from "../domain/comentario";
import {ComentarioMap} from "../mappers/ComentarioMap";
import IPostRepo from "./IRepos/IPostRepo";

@Service()
export default class ComentarioService implements IComentarioService {
  constructor(
    @Inject(config.repos.comentario.name) private comentarioRepo: IComentarioRepo,
    @Inject(config.repos.post.name) private postRepo: IPostRepo
  ) {
  }

  public async getComentario(comentarioId: string): Promise<Result<IComentarioDTO>> {
    try {
      const comentario = await this.comentarioRepo.findByDomainId(comentarioId);

      if (comentario === null) {
        return Result.fail<IComentarioDTO>("Comentario not found");
      } else {
        const comentarioDTOResult = ComentarioMap.toDto(comentario) as IComentarioDTO;
        return Result.ok<IComentarioDTO>(comentarioDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async createComentario(comentarioDto: IComentarioDTO): Promise<Result<IComentarioDTO>> {
    try {
      const post = await this.postRepo.findByDomainId(comentarioDto.post);
      if (post === null) {
        return Result.fail<IComentarioDTO>("the postId passed has parameter is not attributed to any post");
      }

      const comentarioOrError = await Comentario.create(comentarioDto);
      if (comentarioOrError.isFailure) {
        return Result.fail<IComentarioDTO>(comentarioOrError.errorValue());
      }

      const comentarioResult = comentarioOrError.getValue();

      await this.comentarioRepo.save(comentarioResult);

      const comentarioDtoResult = ComentarioMap.toDto(comentarioResult) as IComentarioDTO;
      return Result.ok<IComentarioDTO>(comentarioDtoResult)
    } catch (e) {
      throw e;
    }
  }

  public async updateComentario(comentarioDto: IComentarioDTO): Promise<Result<IComentarioDTO>> {
    try {
      const comentario = await this.comentarioRepo.findByDomainId(comentarioDto.id);

      if (comentario === null) {
        return Result.fail<IComentarioDTO>("Comentario not found");
      } else {
        comentario.text = comentarioDto.text;
        await this.comentarioRepo.save(comentario);

        const comentarioDtoResult = ComentarioMap.toDto(comentario) as IComentarioDTO;
        return Result.ok<IComentarioDTO>(comentarioDtoResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async getAllComentarios(): Promise<Result<IComentarioDTO[]>> {
    try {
      const comentarios = await this.comentarioRepo.findAll();

      if (comentarios === null) {
        return Result.fail<IComentarioDTO[]>("Comentarios not found");
      } else {
        let listComentariosDtoResult: IComentarioDTO[] = [];
        comentarios.forEach(c => {
          listComentariosDtoResult.push(ComentarioMap.toDto(c) as IComentarioDTO);
        });
        return Result.ok<IComentarioDTO[]>(listComentariosDtoResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async getAllComentariosFromPost(postId: string): Promise<Result<IComentarioDTO[]>> {
    try {
      const comentarios = await this.comentarioRepo.findAllFromPost(postId);

      if (comentarios === null) {
        return Result.fail<IComentarioDTO[]>("Comentarios not found");
      } else {
        let listComentariosDtoResult: IComentarioDTO[] = [];
        comentarios.forEach(c => {
          listComentariosDtoResult.push(ComentarioMap.toDto(c) as IComentarioDTO);
        });
        return Result.ok<IComentarioDTO[]>(listComentariosDtoResult);
      }
    } catch (e) {
      throw e;
    }
  }

}
