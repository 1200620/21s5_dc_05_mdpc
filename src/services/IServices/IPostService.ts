import { Result } from "../../core/logic/Result";
import IPostDTO from "../../dto/IPostDTO";


export default interface IPostService  {
  getPost(postId: string): Promise<Result<IPostDTO>>;
  createPost(postDTO: IPostDTO): Promise<Result<IPostDTO>>;
  updatePost(postDTO: IPostDTO): Promise<Result<IPostDTO>>;
  getAllPosts(): Promise<Result<IPostDTO[]>>;
  getAllPostsUser(emailUser:string): Promise<Result<IPostDTO[]>>;
}
