import {Result} from "../../core/logic/Result";
import IComentarioDTO from "../../dto/IComentarioDTO";

export default interface IComentarioService {
  createComentario(comentario: IComentarioDTO): Promise<Result<IComentarioDTO>>;

  updateComentario(comentario: IComentarioDTO): Promise<Result<IComentarioDTO>>;

  getComentario(comentarioId: string): Promise<Result<IComentarioDTO>>;

  getAllComentarios():Promise<Result<IComentarioDTO[]>>;

  getAllComentariosFromPost(postId: string):Promise<Result<IComentarioDTO[]>>;
}
