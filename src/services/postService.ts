import { Inject, Service } from "typedi";
import IPostService from "./IServices/IPostService";
import IPostDTO from "../dto/IPostDTO";
import { Result } from "../core/logic/Result";
import config from "../../config";
import IPostRepo from "./IRepos/IPostRepo";
import { PostMap } from "../mappers/PostMap";
import { Post } from "../domain/post";

@Service()
export default class PostService implements IPostService {
  constructor(
    @Inject(config.repos.post.name) private postRepo : IPostRepo
  ) {}

  public async getPost(postId: string): Promise<Result<IPostDTO>> {
    try {
      const post = await this.postRepo.findByDomainId(postId);

      if (post === null) {
        return Result.fail<IPostDTO>("Post not found");
      }else {
        const postDTOResult = PostMap.toDTO(post) as IPostDTO;
        return Result.ok<IPostDTO>(postDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async createPost(postDTO: IPostDTO): Promise<Result<IPostDTO>> {
    try {
      const postOrError = await Post.create(postDTO);

      if (postOrError.isFailure) {
        return Result.fail<IPostDTO>(postOrError.errorValue());
      }

      const postResult = postOrError.getValue();

      await this.postRepo.save(postResult);

      const postDTOResult = PostMap.toDTO(postResult) as IPostDTO;
      return Result.ok<IPostDTO>( postDTOResult )
    } catch (e) {
      throw e;
    }
  }

  public async updatePost(postDTO: IPostDTO): Promise<Result<IPostDTO>> {
    try {
      const post = await this.postRepo.findByDomainId(postDTO.id);

      if (post === null) {
        return Result.fail<IPostDTO>("Post not found");
      }else {
        post.text = postDTO.text;
        post.likes = postDTO.likes;
        post.dislikes = postDTO.dislikes;
        await this.postRepo.save(post);

        const postDTOResult = PostMap.toDTO(post) as IPostDTO;
        return Result.ok<IPostDTO>(postDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async getAllPosts(): Promise<Result<IPostDTO[]>> {
    try {
      const posts = await this.postRepo.findAll();

      if (posts === null) {
        return Result.fail<IPostDTO[]>("Posts not found");
      } else {
        let listPostsDtoResult: IPostDTO[] = [];
        posts.forEach(c => {
          listPostsDtoResult.push(PostMap.toDTO(c) as IPostDTO);
        });
        return Result.ok<IPostDTO[]>(listPostsDtoResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async getAllPostsUser(emailUser: string): Promise<Result<IPostDTO[]>> {
    try {
      const posts = await this.postRepo.findAllSpecificUser(emailUser);

      if (posts === null) {
        return Result.fail<IPostDTO[]>("Posts not found");
      } else {
        let listPostsDtoResult: IPostDTO[] = [];
        posts.forEach(c => {
          listPostsDtoResult.push(PostMap.toDTO(c) as IPostDTO);
        });
        return Result.ok<IPostDTO[]>(listPostsDtoResult);
      }
    } catch (e) {
      throw e;
    }
  }

}
