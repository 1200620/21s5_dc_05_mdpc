export default interface IComentarioDTO {
  id: string;
  emailUser: string;
  text: string;
  post: string;
}
