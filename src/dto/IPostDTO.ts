import { Comentario } from "../domain/comentario";

export default interface IPostDTO {
  id: string;
  emailUser: string;
  text: string;
  likes: string[];
  dislikes: string[];
  //comentarios: Comentario[];
}
