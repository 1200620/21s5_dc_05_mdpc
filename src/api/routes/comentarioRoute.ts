import { Router } from "express";
import { Container } from "typedi";
import config from "../../../config";
import IComentarioController from "../../controllers/IControllers/IComentarioController";
import {celebrate, Joi} from "celebrate";

const route = Router();

export default (app: Router) => {
  app.use('/comentarios', route);

  const ctrl = Container.get(config.controllers.comentario.name) as IComentarioController;

  route.post('',
    celebrate({
      body: Joi.object({
        emailUser: Joi.string().required(),
        text: Joi.string().required(),
        post:Joi.string().required()
      })
    }),
    (req, res, next) => ctrl.createComentario(req, res, next));

  route.get('/getAll',
    celebrate({
      body: Joi.object()
    }),
    (req, res, next) => ctrl.getAllComentarios(req, res, next));

  route.put('/:id',
    celebrate({
      body: Joi.object({
        emailUser: Joi.string().required(),
        text: Joi.string().required(),
        post:Joi.string().required(),
      }),
    }),
    (req, res, next) => ctrl.updateComentario(req, res, next));

  route.get('/:id',
    celebrate({
      body: Joi.object({
      })
    }),
    (req, res, next) => ctrl.getComentarioById(req, res, next));

  route.get('/getAllComentariosFromPost/:postId',
    celebrate({
      body: Joi.object({
      })
    }),
    (req, res, next) => ctrl.getAllComentariosFromPost(req, res, next));
}
