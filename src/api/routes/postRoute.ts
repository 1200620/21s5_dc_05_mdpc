import { Router } from "express";
import {Container} from 'typedi';
import config from "../../../config";
import IPostController from "../../controllers/IControllers/IPostController";
import { celebrate, Joi } from "celebrate";

const route = Router();

export default (app: Router) => {
  app.use('/posts', route);

  const ctrl = Container.get(config.controllers.post.name) as IPostController;

  route.post('',
    celebrate({
      body: Joi.object({
        emailUser: Joi.string().required(),
        text: Joi.string().required(),
        likes: Joi.array().items(Joi.string()).required(),
        dislikes:Joi.array().items(Joi.string()).required(),
      })
    }),
    (req, res, next) => ctrl.createPost(req, res, next));

  route.get('/getAll',
    celebrate({
      body: Joi.object()
    }),
    (req, res, next) => ctrl.getAllPosts( req, res, next));

  route.put('/:id',
    celebrate({
      body: Joi.object({
        emailUser: Joi.string().required(),
        text: Joi.string().required(),
        likes: Joi.array().items(Joi.string()).required(),
        dislikes:Joi.array().items(Joi.string()).required(),
      }),
    }),
    (req, res, next) => ctrl.updatePost(req, res, next));

  route.get('/:id',
    celebrate({
      body: Joi.object({
      })
    }),
    (req, res, next) => ctrl.getPostById(req, res, next));

  route.get('/getAllPostsUser/:emailUser',
    celebrate({
      body: Joi.object()
    }),
    (req, res, next) => ctrl.getAllPostsFromUser( req, res, next));

}
