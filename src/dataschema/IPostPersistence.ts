export interface IPostPersistence {
  domainId: string;
  emailUser: string;
  text: string;
  likes: string[];
  dislikes: string[];
}
