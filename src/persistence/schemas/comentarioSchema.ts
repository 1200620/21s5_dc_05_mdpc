import mongoose from "mongoose";
import {IComentarioPersistence} from "../../dataschema/IComentarioPersistence";

const ComentarioSchema = new mongoose.Schema(
  {
    domainId: {type:String,unique: true},
    emailUser: { type: String, unique:false},
    text: {type: String, unique:false},
    post:{type:String, unique:false},
  },
  {
    timestamps:true
  }
);

export default mongoose.model<IComentarioPersistence & mongoose.Document>('Comentario', ComentarioSchema);
