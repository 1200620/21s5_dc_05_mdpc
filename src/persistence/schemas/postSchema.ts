import mongoose from "mongoose";
import { IPostPersistence } from "../../dataschema/IPostPersistence";


const PostSchema = new mongoose.Schema(
  {
    domainId: {type:String,unique: true},
    emailUser: { type: String, unique:false},
    text: {type: String, unique:false},
    likes: {type: [String], unique:false},
    dislikes: {type: [String], unique:false}
  },
  {
    timestamps:true
  }
);

export default mongoose.model<IPostPersistence & mongoose.Document>('Post', PostSchema);
