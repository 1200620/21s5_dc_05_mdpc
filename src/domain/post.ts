import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { PostId } from "./postId";
import { Result } from "../core/logic/Result";
import IPostDTO from "../dto/IPostDTO";

interface PostProps {
  emailUser: string;
  text: string;
  likes: string[];
  dislikes: string[];
}

export class Post extends AggregateRoot<PostProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get postId(): PostId {
    return PostId.caller(this.id)
  }

  get emailUser(): string {
    return this.props.emailUser;
  }

  get text(): string {
    return this.props.text;
  }

  set text(text: string){
    this.props.text = text;
  }

  get likes(): string[] {
    return this.props.likes;
  }

  set likes(likes: string[]){
    this.props.likes = likes;
  }

  get dislikes(): string[]{
    return this.props.dislikes;
  }

  set dislikes(dislikes: string[]){
    this.props.dislikes = dislikes;
  }

  private constructor (props: PostProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (postDTO: IPostDTO, id?: UniqueEntityID): Result<Post> {
    const emailUser = postDTO.emailUser;
    const text = postDTO.text;
    const likes = postDTO.likes;
    const dislikes = postDTO.dislikes;

    if (!!emailUser === false || emailUser.length === 0 ||
      !!text === false || text.length === 0) {
      return Result.fail<Post>('Must provide a Post emailUser')
    } else {
      const post = new Post({ emailUser: emailUser, text: text, likes: likes, dislikes: dislikes}, id);
      return Result.ok<Post>( post );
    }
  }

}
