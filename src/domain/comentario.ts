import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ComentarioId } from "./comentarioId";
import { Result } from "../core/logic/Result";
import { AggregateRoot } from "../core/domain/AggregateRoot";
import IComentarioDTO from "../dto/IComentarioDTO";

interface ComentarioProps {
  emailUser: string;
  text: string;
  post: string;
}

export class Comentario extends AggregateRoot<ComentarioProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get comentarioId(): ComentarioId {
    return ComentarioId.caller(this.id)
  }

  get emailUser(): string{
    return this.props.emailUser;
  }

  get text(): string{
    return this.props.text;
  }

  set text(texto: string) {
    this.props.text = texto;
  }

  get post(): string {
    return this.props.post;
  }

  private constructor (props: ComentarioProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (comentarioDto: IComentarioDTO, id?: UniqueEntityID): Result<Comentario> {
    const emailUser = comentarioDto.emailUser;
    const text = comentarioDto.text;
    const post = comentarioDto.post;

    if (!!emailUser === false || emailUser.length === 0 ||
      !!text === false || text.length === 0) {
      return Result.fail<Comentario>('Must provide a Comentario emailUser')
    } else {
      const comentario = new Comentario({ emailUser: emailUser, text: text, post: post }, id);
      return Result.ok<Comentario>( comentario );
    }
  }

}
