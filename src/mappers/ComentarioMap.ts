import {Mapper} from "../core/infra/Mapper";
import {Document, Model} from 'mongoose';
import {UniqueEntityID} from "../core/domain/UniqueEntityID";
import {Comentario} from "../domain/comentario";
import {IComentarioPersistence} from "../dataschema/IComentarioPersistence";
import IComentarioDTO from "../dto/IComentarioDTO";

export class ComentarioMap extends Mapper<Comentario> {

  public static toDto(comentario: Comentario) {
    return {
      id: comentario.id.toString(),
      emailUser: comentario.emailUser,
      text: comentario.text,
      post: comentario.post,
    } as IComentarioDTO;
  }

  public static toDomain(comentario: any | Model<IComentarioPersistence & Document>): Comentario {
    const comentarioOrError = Comentario.create(
      comentario,
      new UniqueEntityID(comentario.domainId)
    );

    comentarioOrError.isFailure ? console.log(comentarioOrError.error) : '';

    return comentarioOrError.isSuccess ? comentarioOrError.getValue() : null;
  }

  public static toPersistence(comentario: Comentario): any {
    return {
      domainId: comentario.id.toString(),
      emailUser: comentario.emailUser,
      text: comentario.text,
      post: comentario.post,
    }
  }

}
