import { Inject, Service } from "typedi";
import IPostRepo from "../services/IRepos/IPostRepo";
import { FilterQuery, Model } from "mongoose";
import { IPostPersistence } from "../dataschema/IPostPersistence";
import { Post } from "../domain/post";
import { PostId } from "../domain/postId";
import { PostMap } from "../mappers/PostMap";


@Service()
export default class PostRepo implements IPostRepo {
  private models: any;

  constructor(
    @Inject('postSchema') private postSchema: Model<IPostPersistence & Document>,
  ) {
  }

  public async exists(post: Post): Promise<boolean> {
    const idX = post.id instanceof PostId ? (<PostId>post.id).toValue() : post.id;

    const query = {domainId: idX};

    const postDocument = await this.postSchema.findOne(query as FilterQuery<IPostPersistence & Document>);

    return !!postDocument === true;
  }

  public async findByDomainId(postId: PostId | string): Promise<Post> {
    const query = { domainId: postId};
    const postRecord = await this.postSchema.findOne( query as FilterQuery<IPostPersistence & Document> );

    if( postRecord != null) {
      return PostMap.toDomain(postRecord);
    }
    else
      return null;
  }

  public async save(post: Post): Promise<Post> {
    const query = {domainId: post.id.toString()};

    const postDocument = await this.postSchema.findOne(query);

    try {
      if (postDocument === null) {
        const rawPost: any = PostMap.toPersistence(post);

        const postCreated = await this.postSchema.create(rawPost);

        return PostMap.toDomain(postCreated);
      } else {
        postDocument.text = post.text;
        postDocument.likes = post.likes;
        postDocument.dislikes = post.dislikes;
        await postDocument.save();

        return post;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findAll(): Promise<Post[]> {
    try {
      var postList: Post[] = [];

      const list = await this.postSchema.find();

      list.forEach(post => {
        postList.push(PostMap.toDomain(post));
      });
      return postList;
    }
    catch(e){
      throw e;
    }
  }

  public async findAllSpecificUser(email:string): Promise<Post[]> {
    try {
      const query = {emailUser:email};
      var postList: Post[] = [];

      const list = await this.postSchema.find(query as FilterQuery<IPostPersistence & Document>);

      list.forEach(post => {
        postList.push(PostMap.toDomain(post));
      });
      return postList;
    }
    catch(e){
      throw e;
    }
  }

}
