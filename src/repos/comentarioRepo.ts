import {Inject, Service} from "typedi";
import {Document, FilterQuery, Model} from "mongoose";
import {PostId} from "../domain/postId";
import IComentarioRepo from "../services/IRepos/IComentarioRepo";
import {IComentarioPersistence} from "../dataschema/IComentarioPersistence";
import {Comentario} from "../domain/comentario";
import {ComentarioId} from "../domain/comentarioId";
import {ComentarioMap} from "../mappers/ComentarioMap";

@Service()
export default class ComentarioRepo implements IComentarioRepo {
  private models: any;

  constructor(
    @Inject('comentarioSchema') private comentarioSchema: Model<IComentarioPersistence & Document>,
  ) {
  }

  public async exists(comentario: Comentario): Promise<boolean> {
    const idX = comentario.id instanceof PostId ? (<ComentarioId>comentario.id).toValue() : comentario.id;

    const query = {domainId: idX};

    const comentarioDocument = await this.comentarioSchema.findOne(query as FilterQuery<IComentarioPersistence & Document>);

    return !!comentarioDocument === true;
  }

  public async findByDomainId(comentarioId: ComentarioId | string): Promise<Comentario> {
    const query = {domainId: comentarioId};
    const comentarioRecord = await this.comentarioSchema.findOne(query as FilterQuery<IComentarioPersistence & Document>);

    if (comentarioRecord != null) {
      return ComentarioMap.toDomain(comentarioRecord);
    } else
      return null;
  }

  public async save(comentario: Comentario): Promise<Comentario> {
    const query = {domainId: comentario.id.toString()};

    const comentarioDocument = await this.comentarioSchema.findOne(query);

    try {
      if (comentarioDocument === null) {
        const rawComentario: any = ComentarioMap.toPersistence(comentario);

        const comentarioCreated = await this.comentarioSchema.create(rawComentario);

        return ComentarioMap.toDomain(comentarioCreated);
      } else {
        comentarioDocument.text = comentario.text;
        await comentarioDocument.save();

        return comentario;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findAll(): Promise<Comentario[]> {
    try {
      var comentariosList: Comentario[] = [];

      const list = await this.comentarioSchema.find();

      list.forEach(c => {
        comentariosList.push(ComentarioMap.toDomain(c));
      });
      return comentariosList;
    }
    catch(e){
      throw e;
    }
  }

  public async findAllFromPost(postId:string): Promise<Comentario[]> {
    try {
      var comentariosList: Comentario[] = [];

      const list = await this.comentarioSchema.find({post:postId});

      list.forEach(c => {
        comentariosList.push(ComentarioMap.toDomain(c));
      });
      return comentariosList;
    }
    catch(e){
      throw e;
    }
  }

}
